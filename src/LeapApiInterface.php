<?php

namespace Drupal\leap_ai;

use Drupal\media\MediaInterface;

/**
 * Provides an interface defining a leap ai image entity type.
 */
interface LeapApiInterface {

  /**
   * Generate image from media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity to generate the image from.
   *
   * @return \Drupal\media\MediaInterface
   *   Decoded json response.
   */
  public function generateFromMedia(MediaInterface $media): MediaInterface;

  /**
   * Send an API request to generate an image.
   *
   * @param $model
   *   String model id.
   * @param $params
   *   Array of request body parameters.
   *
   * @return array
   *   Decoded json response.
   */
  public function generate($model, $params): array;

  /**
   * Update media entity fields based on the API response.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity to update.
   * @param $result
   *   Decoded json response.
   *
   * @return \Drupal\media\MediaInterface
   *   Media entity with updated fields.
   */
  public function fillMediaFields(MediaInterface $media, $result): MediaInterface;

  /**
   * Query the API for the status of the image generation.
   *
   * @param \Drupal\media\MediaInterface $media
   *  Media entity to update.
   *
   * @return \Drupal\media\MediaInterface
   *   Media entity with updated fields.
   */
  public function update(MediaInterface $media) : MediaInterface;

}
