<?php

namespace Drupal\leap_ai\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\leap_ai\LeapApi;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for BigPipe module routes.
 */
class LeapAiController implements ContainerInjectionInterface {

  /**
   * @var \Drupal\leap_ai\LeapApi $leapApi
   */
  protected $leapApi;

  /**
   * Creates an EntityViewController object.
   *
   * @param \Drupal\leap_ai\LeapApi $leap_api
   */
  public function __construct(LeapApi $leap_api) {
    $this->leapApi = $leap_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('leap_ai.leap_api'),
    );
  }

  public function queryStatus(MediaInterface $media) {
    $this->leapApi->update($media);
    $url = Url::fromRoute('entity.media.canonical', ['media' => $media->id()]);
    return new RedirectResponse($url->toString());
  }

}
