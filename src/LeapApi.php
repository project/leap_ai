<?php

namespace Drupal\leap_ai;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media\MediaInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides an interface defining a Leap AI API handler.
 */
class LeapApi implements LeapApiInterface {

  /**
   * The HTTP client instance.
   *
   * @var \GuzzleHttp\ClientInterface $httpClient
   */
  protected $httpClient;

  /**
   * Leap AI API token.
   *
   * @var string
   */
  protected $token;

  /**
   * UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Constructs a new LeapApi object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   UUID service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service) {
    $this->httpClient = $http_client;
    $this->token = $config_factory->get('leap_ai.settings')->get('api_key');
    $this->uuid = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromMedia(MediaInterface $media): MediaInterface {
    $model = $media->getSource()->getConfiguration()['model_id'];
    // TODO:   webhookUrl: "https://your-api-endpoint.com/webhook"

    $result = $this->generate($model, [
        "prompt" => $media->get('leap_prompt')->value,
        "negativePrompt" => $media->get('leap_negative_prompt')->value,
    ] + $this->defaultParams($media->getSource()->getConfiguration()));
    return $this->fillMediaFields($media, $result);
  }

  /**
   * {@inheritdoc}
   */
  public function generate($model, $params): array {
    $response = $this->httpClient->post('https://api.tryleap.ai/api/v1/images/models/' . $model . '/inferences', [
      'body' => json_encode($params, JSON_NUMERIC_CHECK),
      'headers' => [
        'accept' => 'application/json',
        'content-type' => 'application/json',
        'authorization' => 'Bearer ' . $this->token,
      ],
    ]);

    return json_decode($response->getBody()->getContents(), true);
  }

  /**
   * Returns default parameters for the API request.
   *
   * @param $config
   *   Media source configuration.
   *
   * @return array
   *   Array of default parameters.
   */
  protected function defaultParams($config) {
    return [
      'steps' => ($config['steps']),
      'width' => ($config['width']),
      'height' => ($config['height']),
      'numberOfImages' => ($config['number_of_images']),
      'promptStrength' => $config['prompt_strength'],
      'enhancePrompt' => boolval($config['enhance_prompt']),
      'restoreFaces' => boolval($config['restore_faces']),
      'upscaleBy' => 'x' . $config['upscale_by'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fillMediaFields(MediaInterface $media, $result): MediaInterface {
    $model = $media->getSource()->getConfiguration()['model_id'];

    $media->set('leap_id', $result['id'] ?? '');
    $media->set('leap_state', $result['state'] ?? 'queued');
    $media->set('leap_prompt', $result['prompt'] ?? '');
    $media->set('leap_negative_prompt', $result['negativePrompt'] ?? '');
    $media->set('leap_seed', $result['seed'] ?? '');
    $media->set('leap_width', $result['width'] ?? '');
    $media->set('leap_height', $result['height'] ?? '');
    $media->set('leap_number_of_images', $result['numberOfImages'] ?? '');
    $media->set('leap_steps', $result['steps'] ?? '');
    $media->set('leap_weights_id', $result['weightsId'] ?? '');
    $media->set('leap_workspace_id', $result['workspaceId'] ?? '');
    $media->set('leap_created_at', $result['createdAt'] ?? '');
    $media->set('leap_prompt_strength', $result['promptStrength'] ?? '');
    $media->set('leap_model_id', $result['modelId'] ?? '');
    $media->set('leap_upscaling_option', $result['upscalingOption'] ?? '');
    $media->set('leap_sampler', $result['sampler'] ?? '');
    $media->set('leap_is_deleted', $result['isDeleted'] ?? '');
    $media->set('leap_routed_to_queue', $result['routedToQueue'] ?? '');
    $media->set('leap_created_with_api', $result['createdWithApi'] ?? '');

    $images = [];
    foreach ($result['images'] as $item) {
      $filename = sprintf('leap_ai_%s_%s', $model, basename($item['uri']));
      $image = system_retrieve_file($item['uri'], 'public://leap_ai/'.$filename, TRUE);
      $images[] = [
        'target_id' => $image->id(),
        'alt' => $media->get('leap_prompt')->value,
      ];
    }

    $media->set('leap_images', $images);
    $source_field_name = $media->getSource()->getSourceFieldDefinition($media->bundle->entity)->getName();
    $media->set($source_field_name, $image);
    $media->set('name', $media->get('leap_prompt')->value);
    $media->set('thumbnail', $image);
    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function update(MediaInterface $media): MediaInterface {
    $model = $media->getSource()->getConfiguration()['model_id'];

    $inference_id = $media->get('leap_id')->value;
    $response = $this->httpClient->get('https://api.tryleap.ai/api/v1/images/models/'.$model.'/inferences/' . $inference_id, [
      'headers' => [
        'accept' => 'application/json',
        'content-type' => 'application/json',
        'authorization' => 'Bearer ' . $this->token,
      ],
    ]);

    $result = json_decode($response->getBody()->getContents(), TRUE);

    return $this->fillMediaFields($media, $result);
  }
}
