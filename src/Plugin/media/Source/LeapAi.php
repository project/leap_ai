<?php

namespace Drupal\leap_ai\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\Plugin\media\Source\Image;

/**
 * Provides media type plugin for remote images.
 *
 * @MediaSource(
 *   id = "leap_ai",
 *   label = @Translation("Leap Ai"),
 *   description = @Translation("Generate images using Leap Ai API."),
 *   allowed_field_types = {"image"},
 *   default_thumbnail_filename = "remote-image.png"
 * )
 */
class LeapAi extends Image implements MediaSourceFieldConstraintsInterface {

  const TARGET_ENTITY_TYPE = 'leap_ai_image';


  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'id' => $this->t('Leap Ai UUID'),
      'state' => $this->t('State of the image generation'),
      'prompt' => $this->t('Prompt for the image generation'),
      'negative_prompt' => $this->t('Negative aspects to avoid in the image generation'),
      'seed' => $this->t('Seed for the random number generator'),
      'width' => $this->t('Width of the generated image'),
      'height' => $this->t('Height of the generated image'),
      'number_of_images' => $this->t('Number of images generated'),
      'steps' => $this->t('Steps in the image generation'),
      'weights_id' => $this->t('ID for the weights used in image generation'),
      'workspace_id' => $this->t('Workspace ID'),
      'created_at' => $this->t('Timestamp of when the image generation was created'),
      'prompt_strength' => $this->t('Strength of the image generation prompt'),
      'images' => $this->t('Images generated'),
      'model_id' => $this->t('ID of the model used for image generation'),
      'upscaling_option' => $this->t('Upscaling option for the image'),
      'sampler' => $this->t('Sampler used for image generation'),
      'is_deleted' => $this->t('Flag indicating if the image generation is deleted'),
      'routed_to_queue' => $this->t('Queue to which the image generation was routed'),
      'created_with_api' => $this->t('Flag indicating if the image generation was created with an API')
    ];
  }

  /**
   * Get the source field options for the media type form.
   *
   * This returns all fields related to media entities, filtered by the allowed
   * field types in the media source annotation.
   *
   * @return string[]
   *   A list of source field options for the media type form.
   */
  protected function getSourceFieldOptions() {
    return [];
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();
    $form['source_field_message'] = [
      '#markup' => $this->t('The configuration below will be used in the Leap Ai request when creating media entities.'),
    ];

    $public_models = $this->configFactory->get('leap_ai.settings')->get('public_models');

    $model_options = [];

    foreach ($public_models as $public_model) {
      $model_options[$public_model['uuid']] = $public_model['name'];
    }

    $form['model_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Model ID'),
      '#default_value' => $configuration['model_id'],
      '#options' => $model_options,
      '#description' => $this->t('The ID of the model to use to generate images.'),
    ];

    $form['steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Steps'),
      '#default_value' => $configuration['steps'],
      '#description' => $this->t('The number of steps to use for the inference.'),
      '#required' => TRUE,
    ];

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $configuration['width'],
      '#description' => $this->t('The width of the image to use for the inference. Must be a multiple of 8.'),
      '#required' => TRUE,
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $configuration['height'],
      '#description' => $this->t('The number of steps to use for the inference.'),
      '#required' => TRUE,
    ];

    $form['number_of_images'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of images'),
      '#default_value' => $configuration['number_of_images'],
      '#description' => $this->t('The number of images to generate for the inference. Max batch size is 20.'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 20,
    ];

    $form['prompt_strength'] = [
      '#type' => 'number',
      '#title' => $this->t('Prompt strength'),
      '#default_value' => $configuration['prompt_strength'],
      '#description' => $this->t('The higher the prompt strength, the closer the generated image will be to the prompt. Must be between 0 and 30.'),
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 30,
    ];

    $form['restore_faces'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restore faces'),
      '#default_value' => $configuration['restore_faces'],
      '#description' => $this->t('Optionally apply face restoration to the generated images. This will make images of faces look more realistic.'),
    ];

    $form['enhance_prompt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enhance prompt'),
      '#default_value' => $configuration['enhance_prompt'],
      '#description' => $this->t('Optionally enhance your prompts automatically to generate better results.'),
    ];

    $form['upscale_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Upscale by'),
      '#description' => $this->t('Optionally upscale the generated images. This will make the images look more realistic. The default is x1, which means no upscaling. The maximum is x4.'),
      '#default_value' => $configuration['upscale_by'],
      '#options' => [
        '1' => $this->t('1x'),
        '2' => $this->t('2x'),
        '3' => $this->t('3x'),
        '4' => $this->t('4x'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    if ($configuration['number_of_images'] > 20) {
      $form_state->setErrorByName('number_of_images', $this->t('The number of images must be less than 20.'));
    }
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'model_id' => '8b1b897c-d66d-45a6-b8d7-8e32421d02cf',
        'steps' => 50,
        'width' => 512,
        'height' => 512,
        'number_of_images' => 1,
        'prompt_strength' => 7,
        'enhance_prompt' => FALSE,
        'restore_faces' => TRUE,
        'upscale_by' => "x1"
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [];
  }
  /**
   * Creates the source field storage definition.
   *
   * By default, the first field type listed in the plugin definition's
   * allowed_field_types array will be the generated field's type.
   *
   * @return \Drupal\field\FieldStorageConfigInterface
   *   The unsaved field storage definition.
   */
  protected function createSourceFieldStorage() {
    return parent::createSourceFieldStorage()->setSettings([
      'target_type' => static::TARGET_ENTITY_TYPE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {

    $this->additionalFieldMappings($type);
    return parent::createSourceField($type)
      ->set('settings', [
        'handler' => 'default',
      ])
      ->set('label', 'Selected image');
  }

  protected function additionalFieldMappings(MediaTypeInterface $type) {
    $field_type_overrides = [
      'width' => 'integer',
      'height' => 'integer',
      'number_of_images' => 'integer',
      'steps' => 'integer',
      'id' => 'uuid',
      'prompt' => 'string_long',
      'negative_prompt' => 'string_long',
      'created_at' => 'timestamp',
      'prompt_strength' => 'integer',
      'is_deleted' => 'boolean',
      'created_with_api' => 'boolean'
    ];

    foreach ($this->getMetadataAttributes() as $attribute => $label) {

      if ($attribute === 'images') {
        $this->createAdditionalSourceField(
          $attribute,
          $type,
          'image',
          $label,
          FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
        );
        continue;
      }

      $this->createAdditionalSourceField(
        $attribute,
        $type,
        $field_type_overrides[$attribute] ?? 'string',
        $label
      );
    }

  }

  protected function createAdditionalSourceField($field_name, MediaTypeInterface $type, $field_type, $label, $cardinality = 1) {
    $field_name = 'leap_' . $field_name;

    $storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions('media');

    $storage = $storage_definitions[$field_name] ?? $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->create([
        'entity_type' => 'media',
        'field_name' => $field_name,
        'type' => $field_type,
        'cardinality' => $cardinality,
      ]);

    $field = $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $label,
        'required' => TRUE,
      ]);

    $storage = $field->getFieldStorageDefinition();
    if ($storage->isNew()) {
      $storage->save();
    }
    $field->save();
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);

    $weight = $display->getComponent($this->getSourceFieldDefinition($type)->getName())['weight'];
    $display->setComponent('leap_prompt', [
      'type' => 'string_textarea',
      'weight' => $weight,
      'settings' => [
        'rows' => 2,
      ],
    ]);

    $display->setComponent('leap_negative_prompt', [
      'type' => 'string_textarea',
      'weight' => $weight + 1,
      'settings' => [
        'rows' => 2,
      ],
    ]);

    $display->removeComponent($this->getSourceFieldDefinition($type)->getName());
    $display->removeComponent('name');
  }

}
